1A) 
Given a list L of 32 signed Integers each of 16 bits, sort them using Bubble Sort. The 32
integers will be stored in a data section of your code i.e store these integers in memory not in
registers. You need to report the sorted list of integers.

2A)
Given list L of 32 signed Integers each of 16 bits and a key X (16 bit). Find the location of X
within L, return -1 if X does not belong to L. The list L is stored in memory inside the data
section. You have to use Sequential Search to look for X within L. Report the number of
iterations taken to successfully/unsuccessfully find X.

3B)
Given a Matrix M with 16 bit values in memory. M has 8 rows and 8 columns. Rotate the
matrix by 90 degrees in the clockwise direction without using any extra memory.

4B)
Given a character array C of length 32. Count the frequency of each capital letter ‘A’ to ‘Z’
appearing in C. Report 0 in case an alphabet does not appear in C.

5B)
Given two 32 bit unsigned integers A and B. Find the hamming distance between them and
store the result in another integer C.
